#! /usr/bin/bash

cat /dev/ttyUSB0 > /dev/null 3>&1 & disown

while [[ true ]]; do
	sensorsOutput=$(sensors)
	cpuTemp=${sensorsOutput:53:2}
	gpuTemp=${sensorsOutput:374:2}
	ramUsed=$(echo $(free -m | awk '{print $3}') | awk '{print $2}')

	echo "cpu="${cpuTemp} > /dev/ttyUSB0
	echo "gpu="${gpuTemp} > /dev/ttyUSB0
	echo "mem="${ramUsed} > /dev/ttyUSB0

	sleep 3
done
