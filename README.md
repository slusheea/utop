# µtop

![µtop](/images/utop.png/)

### Display your PC's stats inside it's own case

MicroTop displays CPU temperature, GPU temperature and memory usage on an LCD inside a computer case.

## utop.sh

`utop.sh` uses [lm_sensors](https://github.com/lm-sensors/lm-sensors) and [free](https://www.man7.org/linux/man-pages/man1/free.1.html) to gather information. Each computer will print the `lm_sensors` information on a different spot, so you'll have to edit the slices:
```
cpuTemp=${sensorsOutput:[HERE]:2}
gpuTemp=${sensorsOutput:[HERE]:2}
```
`free` should print the used memory on the same field independently of the computer it's being run on. But I would recommend checking if the output of `free -m | awk '{print $3}') | awk '{print $2}'` is correct just in case.

It sends the information by doing `echo` to the device in `/dev/`. You'll have to edit it so it sends it to your microcontroller:
```
echo "cpu="${cpuTemp} > /dev/[HERE]
echo "gpu="${gpuTemp} > /dev/[HERE]
echo "mem="${ramUsed} > /dev/[HERE]
```
You can also set how often it updates by changing the sleep time:
```
sleep [HERE]
```
Make sure the user is in the `dialout` or `uucp` group.

## Microcontroller 

This is made for an arduino nano, but it should work on the micro, uno, mega, leonardo and yun. (Tested on the nano and uno). It uses the [Liquidcrystal I2C](https://www.arduino.cc/reference/en/libraries/liquidcrystal-i2c/) library configured for a 2\*16 display. It will turn the screen off if it doesn't recieve any data in ten seconds. You can change this time:
```
const long timeoutTime = [HERE]; // time in milliseconds
```

![gif](/images/use.gif)
