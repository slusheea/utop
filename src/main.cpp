#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

// Defines LCD as a 16*2
LiquidCrystal_I2C lcd(0x27,20,4);

unsigned long currentTime = millis();
unsigned long previousTime = 0;

// Time in ms. If this time has passed and no data has been recieved,
// the screen will turn off.
const long timeoutTime = 10000;

int cpu = 0;
int gpu = 0;
int mem = 0;

void membar(int mem, int i) {
	byte m_bar_0[8] = { 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F };    
	byte m_bar_1[8] = { 0x1F, 0x00, 0x10, 0x10, 0x10, 0x10, 0x00, 0x1F };	
	byte m_bar_2[8] = { 0x1F, 0x00, 0x18, 0x18, 0x18, 0x18, 0x00, 0x1F };	
	byte m_bar_3[8] = { 0x1F, 0x00, 0x1C, 0x1C, 0x1C, 0x1C, 0x00, 0x1F };
	byte m_bar_4[8] = { 0x1F, 0x00, 0x1E, 0x1E, 0x1E, 0x1E, 0x00, 0x1F };	
	byte m_bar_5[8] = { 0x1F, 0x00, 0x1F, 0x1F, 0x1F, 0x1F, 0x00, 0x1F };	
	lcd.createChar(0, m_bar_0);
	lcd.createChar(1, m_bar_1);
	lcd.createChar(2, m_bar_2);
	lcd.createChar(3, m_bar_3);
	lcd.createChar(4, m_bar_4);
	lcd.createChar(5, m_bar_5);

	int map_mem = map(mem, 0, 15943, 0, 55);

	if (map_mem < ((i*5) + 1) ) {
		lcd.setCursor(i, 1);
		lcd.write(byte(0));
	} else if (map_mem == ((i*5) + 1)) {
		lcd.setCursor(i, 1);
		lcd.write(byte(1));
	} else if (map_mem == ((i*5) + 2)) {
		lcd.setCursor(i, 1);
		lcd.write(byte(2));
	}  else if (map_mem == ((i*5) + 3)) {
		lcd.setCursor(i, 1);
		lcd.write(byte(3));
	}  else if (map_mem == ((i*5) + 4)) {
		lcd.setCursor(i, 1);
		lcd.write(byte(4));
	}  else if (map_mem > ((i*5) + 4)) {
		lcd.setCursor(i, 1);
		lcd.write(byte(5));
	}
}

void setup() {
	lcd.init();
	lcd.backlight();
	Serial.begin(9600);
}

void loop() {
	String readString;
	String serialData;
	
	// Read the serial port and create a string from the recieved characters
	while (Serial.available()) {
		delay(1);
		if (Serial.available() > 0) {
			char c = Serial.read();
			if (isControl(c)) {
				currentTime = millis();
				previousTime = currentTime;
				break;
      			}
      			readString += c;
    		}
	}

	serialData = readString;
	currentTime = millis();
	
	// Matches the recieved string and assigns the data to the respective variable
	if (serialData.indexOf("cpu=") >= 0) {
		serialData.remove(0, 4);
		cpu = serialData.toInt();
	} else if (serialData.indexOf("gpu=") >= 0) {
		serialData.remove(0, 4);
		gpu = serialData.toInt();
	} else if (serialData.indexOf("mem=") >= 0) {
		serialData.remove(0, 4);
		mem = serialData.toInt();

		// If the timeout time passes and no data has been recieved,
		// the screen is cleared and the backlight turned off
	} else if (currentTime - previousTime >= timeoutTime) {
		lcd.clear();
		lcd.noBacklight();

		// If nothing else is being done it displays the data
	} else {
    		lcd.backlight();
		lcd.setCursor(0, 0);
		lcd.print("CPU: ");
		lcd.setCursor(5, 0);
		lcd.print(cpu);
		lcd.setCursor(9, 0);
		lcd.print("GPU: ");
		lcd.setCursor(14, 0);
		lcd.print(gpu);
		
		// Place the cursor depending on the memory number
		if (mem < 1000) {
			lcd.setCursor(13, 1);
		} else if (mem < 10000) {
			lcd.setCursor(12, 1);
		} else if (mem >= 10000) {
			lcd.setCursor(11, 1);
		}
		
		lcd.print(mem);

		for (int i = 0; i <= 10; i++) {
    			membar(mem, i);
 		}
	}
}
